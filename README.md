# Initialisation

application created with 
``` 
mn create-app vrac.api -l=kotlin -f=graalvm,management
```

# As a Developer
## I want to locally test the api

First you need a postgresql database.

With Docker :

``` 
docker run -it --rm \
    --name vraak-db-test \
    -p 17000:5432 \
    -e POSTGRES_USER=me \
    -e POSTGRES_PASSWORD=easy \
    -e POSTGRES_DB=vraak \
    postgres:11.5-alpine
```

# As an Ops
## I want to build a native image

``` 
./gradlew assemble
native-image --no-server --initialize-at-build-time=org.postgresql.Driver,org.postgresql.util.SharedTimer -cp build/libs/vraak-0.1-all.jar
```
