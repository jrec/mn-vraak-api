package vrac.api.controller

import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.*
import vrac.api.domain.Track
import vrac.api.repository.TrackRepository
import javax.inject.Inject

@Controller("/tracks")
class TracksController {

    @Inject
    lateinit var trackRepository: TrackRepository

    @Get(uri = "/", produces = ["text/plain"])
    fun index(): String {
        return "Example Response"
    }

    @Get(uri = "/list", produces = ["text/plain"])
    fun list(): String {
        return "tracks: " + trackRepository.count()
    }

    @Status(HttpStatus.CREATED)
    @Post
    fun create(@Body track: Track): HttpResponse<Track> {
        trackRepository.save(track);
        return HttpResponse.created(track);
    }
}