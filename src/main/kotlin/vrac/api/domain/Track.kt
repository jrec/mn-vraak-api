package vrac.api.domain

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.util.*
import javax.persistence.*

@Entity
data class Track(
        @Id
        @GeneratedValue
        var id: Long,

        @Column(unique = true)
        var filePath: String,

        @Version
        var version: Int,

        @UpdateTimestamp
        var lastUpdated: Date?,

        @CreationTimestamp
        var dateCreated: Date?
)

