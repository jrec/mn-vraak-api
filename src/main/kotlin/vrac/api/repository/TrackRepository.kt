package vrac.api.repository

import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.CrudRepository
import vrac.api.domain.Track

@Repository
interface TrackRepository : CrudRepository<Track, Long>